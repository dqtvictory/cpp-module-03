#include "ClapTrap.hpp"
#include <iostream>

#define	PREFIX	"[CL4P-TP] "
#define	ATTACK_COST	3

ClapTrap::ClapTrap(std::string name) :	_name(name),
										_hitPoints(10),
										_engPoints(10),
										_attackDmg(0)
{
	std::cout << PREFIX << name << " is born.\n";
}

ClapTrap::ClapTrap(const ClapTrap &ct)
{
	*this = ct;
}

ClapTrap	&ClapTrap::operator=(const ClapTrap &ct)
{
	_name = ct._name;
	_hitPoints = ct._hitPoints;
	_engPoints = ct._engPoints;
	_attackDmg = ct._attackDmg;
	std::cout << PREFIX << _name << "is born.\n";
	return (*this);
}

ClapTrap::~ClapTrap(void)
{
	std::cout << PREFIX << _name << " has been destroyed forever.\n";
}

void	ClapTrap::attack(std::string const &target)
{
	if (_hitPoints == 0)
		return ;

	if (_engPoints < ATTACK_COST)
		std::cout	<< PREFIX << _name << " cannot attack because it's too tired.\n";
	else
	{
		std::cout	<< PREFIX << _name << " attacks " << target
					<< " causing " << _attackDmg << " points of damage!\n";
		_engPoints -= ATTACK_COST;
	}
}

void	ClapTrap::takeDamage(unsigned int amount)
{
	if (_hitPoints == 0)
		return ;

	std::cout	<< PREFIX << _name << " took " << amount
				<< " damage to its face. Ouch!\n";
	if (amount >= _hitPoints)
	{
		_hitPoints = 0;
		std::cout	<< PREFIX << _name << " DIED!\n";
	}
	else
		_hitPoints -= amount;
}

void	ClapTrap::beRepaired(unsigned int amount)
{
	if (_hitPoints == 0)
		return ;

	std::cout	<< PREFIX << _name << " gets repaired and gained "
				<< amount << " HP.\n";
	_hitPoints += amount;
}
