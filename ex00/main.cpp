#include <iostream>
#include "ClapTrap.hpp"

int main()
{
	ClapTrap	ct("Useless trap");

	ct.attack("42 Paris");
	ct.attack("42 Paris");
	ct.attack("42 Paris");
	ct.attack("42 Paris");	// Tired
	ct.takeDamage(6);
	ct.beRepaired(3);
	ct.takeDamage(10);
	
	// From here on nothing is displayed because trap is dead
	ct.beRepaired(5);
	ct.attack("Void");
	ct.takeDamage(1);
}