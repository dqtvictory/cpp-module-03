#include <iostream>
#include "ScavTrap.hpp"

int main()
{
	std::cout << "\n======== CLAP ME ========\n";
	ClapTrap	*ct = new ClapTrap("ClapClap");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");	// Tired
	ct->takeDamage(6);
	ct->beRepaired(3);
	ct->takeDamage(10);

	// From here on nothing is displayed because trap is dead
	ct->beRepaired(5);
	ct->attack("Void");
	ct->takeDamage(1);
	delete ct;


	std::cout << "\n======== SCAV ME ========\n";
	ScavTrap	st("ScavScav");
	st.attack("42 Paris");
	st.attack("42 Paris");	// Tired
	st.takeDamage(80);
	st.beRepaired(10);
	st.guardGate();
	st.takeDamage(35);

	// From here on nothing is displayed because trap is dead
	st.guardGate();
	st.beRepaired(5);
	st.attack("Void");
	st.takeDamage(1);
}