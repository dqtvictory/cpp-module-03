#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <string>
#define NOCOLOR	"\033[0m"


class ClapTrap
{

private:
	static const unsigned int	_defaultHP = 10;
	static const unsigned int	_defaultEP = 10;
	static const unsigned int	_defaultAD = 0;

protected:
	std::string		_name;
	unsigned int	_hitPoints;
	unsigned int	_engPoints;
	unsigned int	_attackDmg;
	ClapTrap(std::string name, unsigned int hp, unsigned int ep, unsigned int dam);

public:
	ClapTrap(std::string name);
	ClapTrap(const ClapTrap &ct);
	ClapTrap	&operator=(const ClapTrap &ct);
	~ClapTrap(void);

	void	attack(std::string const &target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

};

#endif
