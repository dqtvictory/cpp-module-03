#include <iostream>
#include "DiamondTrap.hpp"

#define COLOR	"\033[92m"
#define	PREFIX	"[D14M0ND] "


static std::string	prefix(std::string name)
{
	return (std::string(COLOR) + PREFIX + name);
}

DiamondTrap::DiamondTrap(std::string name) :	ClapTrap(name),
												FragTrap(name),
												ScavTrap(name),
												_name(name)
{
	ClapTrap::_name = name + "_clap_name";
	_hitPoints = _defaultHP;
	_engPoints = _defaultEP;
	_attackDmg = _defaultAD;
	std::cout << prefix(name) << " is born.\n" << NOCOLOR;
}

DiamondTrap::DiamondTrap(const DiamondTrap &st) :	ClapTrap(st),
													FragTrap(st),
													ScavTrap(st)
{
	*this = st;
}

DiamondTrap	&DiamondTrap::operator=(const DiamondTrap &st)
{
	_name = st._name;
	_hitPoints = st._hitPoints;
	_engPoints = st._engPoints;
	_attackDmg = st._attackDmg;
	std::cout << prefix(_name) << " is born.\n" << NOCOLOR;
	return (*this);
}

DiamondTrap::~DiamondTrap(void)
{
	std::cout << prefix(_name) << " has been destroyed forever.\n" << NOCOLOR;
}

void	DiamondTrap::attack(std::string const &target)
{
	ScavTrap::attack(target);
}

void	DiamondTrap::whoAmI(void)
{
	std::cout	<< prefix(_name) << " My name is " << _name
				<< " and my CLAP name is " << ClapTrap::_name << '\n' << NOCOLOR;
}
