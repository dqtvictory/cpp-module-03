#include <iostream>
#include "DiamondTrap.hpp"

int main()
{
	std::cout << "\n======== CLAP ME ========\n";
	ClapTrap	*ct = new ClapTrap("ClapClap");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");	// Tired
	ct->takeDamage(6);
	ct->beRepaired(3);
	ct->takeDamage(10);

	// From here on nothing is displayed because trap is dead
	ct->beRepaired(5);
	ct->attack("Void");
	ct->takeDamage(1);
	delete ct;


	std::cout << "\n======== SCAV ME ========\n";
	ScavTrap	*st = new ScavTrap("ScavScav");
	st->attack("42 Paris");
	st->attack("42 Paris");	// Tired
	st->takeDamage(80);
	st->beRepaired(10);
	st->guardGate();
	st->takeDamage(35);

	// From here on nothing is displayed because trap is dead
	st->guardGate();
	st->beRepaired(5);
	st->attack("Void");
	st->takeDamage(1);
	delete st;


	std::cout << "\n======== FRAG ME ========\n";
	FragTrap	*ft = new FragTrap("FragFrag");
	ft->attack("42 Paris");
	ft->attack("42 Paris");
	ft->attack("42 Paris");	// Tired
	ft->takeDamage(80);
	ft->beRepaired(10);
	ft->highFivesGuys();
	ft->takeDamage(35);

	// From here on nothing is displayed because trap is dead
	ft->highFivesGuys();
	ft->beRepaired(5);
	ft->attack("Void");
	ft->takeDamage(1);
	delete ft;


	std::cout << "\n======== I'M DIAMOND NOW ========\n";
	DiamondTrap	dt("Diamond");
	dt.whoAmI();
	dt.attack("42 Paris");
	dt.attack("42 Paris");	// Tired
	dt.takeDamage(80);
	dt.beRepaired(10);
	dt.guardGate();
	dt.highFivesGuys();
	dt.takeDamage(35);

	// From here on nothing is displayed because trap is dead
	dt.guardGate();
	dt.highFivesGuys();
	dt.beRepaired(5);
	dt.attack("Void");
	dt.takeDamage(1);
}