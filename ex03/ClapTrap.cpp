#include "ClapTrap.hpp"
#include <iostream>

#define COLOR	"\033[95m"
#define	PREFIX	"[CL4P-TP] "

#define ATTACK_COST	3


static std::string	prefix(std::string name)
{
	return (std::string(COLOR) + PREFIX + name);
}

ClapTrap::ClapTrap
(std::string name, unsigned int hp,
unsigned int ep, unsigned int dam):	_name(name),
									_hitPoints(hp),
									_engPoints(ep),
									_attackDmg(dam)
{
	std::cout << prefix(name) << " is born.\n" << NOCOLOR;
}

ClapTrap::ClapTrap(std::string name):	_name(name),
										_hitPoints(_defaultHP),
										_engPoints(_defaultEP),
										_attackDmg(_defaultAD)
{
	std::cout << prefix(name) << " is born.\n" << NOCOLOR;
}

ClapTrap::ClapTrap(const ClapTrap &ct)
{
	*this = ct;
}

ClapTrap	&ClapTrap::operator=(const ClapTrap &ct)
{
	_name = ct._name;
	_hitPoints = ct._hitPoints;
	_engPoints = ct._engPoints;
	_attackDmg = ct._attackDmg;
	std::cout << prefix(_name) << " is born.\n" << NOCOLOR;
	return (*this);
}

ClapTrap::~ClapTrap(void)
{
	std::cout << prefix(_name) << " has been destroyed forever.\n" << NOCOLOR;
}

void	ClapTrap::attack(std::string const &target)
{
	if (_hitPoints == 0)
		return ;

	if (_engPoints < ATTACK_COST)
		std::cout << prefix(_name) << " cannot attack because it's too tired.\n" << NOCOLOR;
	else
	{
		std::cout	<< prefix(_name) << " attacks " << target
					<< " causing " << _attackDmg << " points of damage!\n" << NOCOLOR;
		_engPoints -= ATTACK_COST;
	}
}

void	ClapTrap::takeDamage(unsigned int amount)
{
	if (_hitPoints == 0)
		return ;

	std::cout	<< prefix(_name) << " took " << amount
				<< " damage to its face. Ouch!\n" << NOCOLOR;
	if (amount >= _hitPoints)
	{
		_hitPoints = 0;
		std::cout << prefix(_name) << " DIED.\n" << NOCOLOR;
	}
	else
		_hitPoints -= amount;
}

void	ClapTrap::beRepaired(unsigned int amount)
{
	if (_hitPoints == 0)
		return ;

	std::cout	<< prefix(_name) << " gets repaired and gained "
				<< amount << " HP.\n" << NOCOLOR;
	_hitPoints += amount;
}
