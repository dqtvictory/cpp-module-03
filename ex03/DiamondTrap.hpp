#ifndef DIAMONDTRAP_HPP
#define DIAMONDTRAP_HPP

#include "FragTrap.hpp"
#include "ScavTrap.hpp"


class DiamondTrap : public FragTrap, public ScavTrap
{
private:

	static const unsigned int	_defaultHP = FragTrap::_defaultHP;
	static const unsigned int	_defaultEP = ScavTrap::_defaultEP;
	static const unsigned int	_defaultAD = FragTrap::_defaultAD;
	std::string	_name;

public:

	DiamondTrap(std::string name);
	DiamondTrap(const DiamondTrap &st);
	DiamondTrap	&operator=(const DiamondTrap &st);
	virtual ~DiamondTrap(void);

	virtual void	attack(std::string const &target);
	void			whoAmI(void);

};

#endif
