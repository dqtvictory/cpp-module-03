#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap
{

protected:
	static const unsigned int	_defaultHP = 100;
	static const unsigned int	_defaultEP = 100;
	static const unsigned int	_defaultAD = 30;

public:

	FragTrap(std::string name);
	FragTrap(const FragTrap &st);
	FragTrap	&operator=(const FragTrap &st);
	virtual ~FragTrap(void);

	virtual void	attack(std::string const &target);
	void			highFivesGuys(void);

};

#endif
