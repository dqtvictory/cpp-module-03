#include <iostream>
#include "ScavTrap.hpp"

#define COLOR	"\033[94m"
#define	PREFIX	"[SC4V-TP] "

#define ATTACK_COST	30


static std::string	prefix(std::string name)
{
	return (std::string(COLOR) + PREFIX + name);
}

ScavTrap::ScavTrap(std::string name) : ClapTrap(name, _defaultHP, _defaultEP, _defaultAD)
{
	std::cout << prefix(name) << " is born.\n" << NOCOLOR;
}

ScavTrap::ScavTrap(const ScavTrap &st) : ClapTrap(st)
{
	*this = st;
}

ScavTrap	&ScavTrap::operator=(const ScavTrap &st)
{
	_name = st._name;
	_hitPoints = st._hitPoints;
	_engPoints = st._engPoints;
	_attackDmg = st._attackDmg;
	std::cout << prefix(_name) << " is born.\n" << NOCOLOR;
	return (*this);
}

ScavTrap::~ScavTrap(void)
{
	std::cout << prefix(_name) << " has been destroyed forever.\n" << NOCOLOR;
}

void	ScavTrap::attack(std::string const &target)
{
	if (_hitPoints == 0)
		return ;

	if (_engPoints < ATTACK_COST)
		std::cout << prefix(_name) << " cannot attack because it's too tired.\n" << NOCOLOR;
	else
	{
		std::cout	<< prefix(_name) << " attacks " << target
					<< " causing " << _attackDmg << " points of damage!\n" << NOCOLOR;
		_engPoints -= ATTACK_COST;
	}
}

void	ScavTrap::guardGate(void)
{
	if (_hitPoints == 0)
		return ;

	std::cout << prefix(_name) << " entered Gate keeper mode.\n" << NOCOLOR;
}
