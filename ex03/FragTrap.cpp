#include <iostream>
#include "FragTrap.hpp"

#define COLOR	"\033[93m"
#define	PREFIX	"[FR49-TP] "

#define ATTACK_COST	50


static std::string	prefix(std::string name)
{
	return (std::string(COLOR) + PREFIX + name);
}

FragTrap::FragTrap(std::string name) : ClapTrap(name, _defaultHP, _defaultEP, _defaultAD)
{
	std::cout << prefix(name) << " is born.\n" << NOCOLOR;
}

FragTrap::FragTrap(const FragTrap &st) : ClapTrap(st)
{
	*this = st;
}

FragTrap	&FragTrap::operator=(const FragTrap &st)
{
	_name = st._name;
	_hitPoints = st._hitPoints;
	_engPoints = st._engPoints;
	_attackDmg = st._attackDmg;
	std::cout << prefix(_name) << " is born.\n" << NOCOLOR;
	return (*this);
}

FragTrap::~FragTrap(void)
{
	std::cout << prefix(_name) << " has been destroyed forever.\n" << NOCOLOR;
}

void	FragTrap::attack(std::string const &target)
{
	if (_hitPoints == 0)
		return ;

	if (_engPoints < ATTACK_COST)
		std::cout << prefix(_name) << " cannot attack because it's too tired.\n" << NOCOLOR;
	else
	{
		std::cout	<< prefix(_name) << " attacks " << target
					<< " causing " << _attackDmg << " points of damage!\n" << NOCOLOR;
		_engPoints -= ATTACK_COST;
	}
}

void	FragTrap::highFivesGuys(void)
{
	if (_hitPoints == 0)
		return ;

	std::cout << prefix(_name) << " Wanna highfive?\n" << NOCOLOR;
}
