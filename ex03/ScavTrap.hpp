#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap : virtual public ClapTrap
{

protected:
	static const unsigned int	_defaultHP = 100;
	static const unsigned int	_defaultEP = 50;
	static const unsigned int	_defaultAD = 20;

public:

	ScavTrap(std::string name);
	ScavTrap(const ScavTrap &st);
	ScavTrap	&operator=(const ScavTrap &st);
	virtual ~ScavTrap(void);

	virtual void	attack(std::string const &target);
	void			guardGate(void);

};

#endif
