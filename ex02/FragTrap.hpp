#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{

public:

	FragTrap(std::string name);
	FragTrap(const FragTrap &st);
	FragTrap	&operator=(const FragTrap &st);
	virtual ~FragTrap(void);

	virtual void	attack(std::string const &target);
	void			highFivesGuys(void);

};

#endif
