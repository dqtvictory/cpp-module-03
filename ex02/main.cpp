#include <iostream>
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

int main()
{
	std::cout << "\n======== CLAP ME ========\n";
	ClapTrap	*ct = new ClapTrap("ClapClap");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");
	ct->attack("42 Paris");	// Tired
	ct->takeDamage(6);
	ct->beRepaired(3);
	ct->takeDamage(10);

	// From here on nothing is displayed because trap is dead
	ct->beRepaired(5);
	ct->attack("Void");
	ct->takeDamage(1);
	delete ct;


	std::cout << "\n======== SCAV ME ========\n";
	ScavTrap	*st = new ScavTrap("ScavScav");
	st->attack("42 Paris");
	st->attack("42 Paris");	// Tired
	st->takeDamage(80);
	st->beRepaired(10);
	st->guardGate();
	st->takeDamage(35);

	// From here on nothing is displayed because trap is dead
	st->guardGate();
	st->beRepaired(5);
	st->attack("Void");
	st->takeDamage(1);
	delete st;


	std::cout << "\n======== FRAG ME ========\n";
	FragTrap	ft("FragFrag");
	ft.attack("42 Paris");
	ft.attack("42 Paris");
	ft.attack("42 Paris");	// Tired
	ft.takeDamage(80);
	ft.beRepaired(10);
	ft.highFivesGuys();
	ft.takeDamage(35);

	// From here on nothing is displayed because trap is dead
	ft.highFivesGuys();
	ft.beRepaired(5);
	ft.attack("Void");
	ft.takeDamage(1);
}